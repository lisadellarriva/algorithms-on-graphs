# Algorithms on Graphs

This repository constains projects implemented as part of a course on graph's algorithms.
It is to be better documented soon.

### Theoretical exercises ###
The folder exercicios contains solutions to theoretical assignments.

### Practical projects ###
The folder laboratorios contains small projects where some algorithms on graphs were implemented, in c++ language.
