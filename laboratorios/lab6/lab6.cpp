// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 6: Programacao Linear
// Aluna: Elisa Dell'Arriva - 135551

// Estruturas utilizadas
// Lp::Expr para expressoes do saldo de cada conta, saldoXi
// Lp::Expr para o valor final da heranca, hf

#include <iostream>
#include <ios>
#include <lemon/lp.h>

using namespace lemon;
using namespace std;

// Variaveis globais
// Dados de entrada
int n;                  //numero de casos
double h;               //valor da heranca recebida
double c;               //tarifa mensal da conta corrente
double rp;              //rendimento mensal da poupanca
double tp;              //taxa por saque da poupanca
double tb;              //taxa por saque do CDB
vector<double> rb(12);  //rendimentos mensais do CDB
vector<double> ra(12);  //rendimentos mensais das acoes
vector<double> d(13);   //despesas mensais

// Variaveis do pl
vector<Lp::Col> ap(12);  //valor aplicado na poupanca
vector<Lp::Col> ab(12);  //valor aplicado no CDB
vector<Lp::Col> aa(12);  //valor aplicado em acoes
vector<Lp::Col> sp(13);  //valor sacado da poupanca
vector<Lp::Col> sb(13);  //valor sacado do CDB
vector<Lp::Col> sa(13);  //valor sacado de acoes

// Expressoes
vector<Lp::Expr> saldoP(13);    //saldo da poupanca
vector<Lp::Expr> saldoCC(13);   //saldo da conta corrente
vector<Lp::Expr> saldoCDB(13);  //sado do CDB
vector<Lp::Expr> saldoA(13);    //saldo das acoes
vector<Lp::Expr> dmTotal(13);   //valor da despesa total a cada mes
Lp::Expr hf;                    //valor da heranca final em 1/2019

Lp lp;                          //instancia do programa linear

//define as expressoes e restricoes do pl
void defineLP() {
  // Variaveis do pl (colunas)
  lp.addColSet(ap);
  lp.addColSet(ab);
  lp.addColSet(aa);
  lp.addColSet(sp);
  lp.addColSet(sb);
  lp.addColSet(sa);

  // Expressoes
  saldoP[0] = 0.0;
  saldoCDB[0] = 0.0;
  saldoA[0] = 0.0;
  saldoCC[0] = h;
  dmTotal[0] = d[0];
  for (int i=0; i<12; i++) {
    dmTotal[i+1] = d[i+1] + c;
    saldoCC[i+1] = saldoCC[i] - dmTotal[i] + sp[i+1] + sb[i+1] + sa[i+1] - ap[i] - ab[i] - aa[i];
    saldoP[i+1] = (saldoP[i] + ap[i])*(1+rp/100.00) - sp[i+1]*(1+tp/100.00);
    saldoCDB[i+1] = (saldoCDB[i] + ab[i])*(1+rb[i]/100.00) - sb[i+1]*(1+tb/100.00);
    saldoA[i+1] = (saldoA[i] + aa[i])*(1+ra[i]/100.00) - sa[i+1];
  }
  saldoCC[12] = saldoCC[12] - dmTotal[12];

  hf = saldoCC[12] + saldoP[12] + saldoA[12] + saldoCDB[12];

  // Funcao objetivo
  lp.max();
  lp.obj(hf);

  // RESTRICOES
  //1. nao negatividade das quantias de dinheiro aplicadas e retiradas
  lp.colLowerBound(ap, 0);
  lp.colLowerBound(ab, 0);
  lp.colLowerBound(aa, 0);
  lp.colLowerBound(sp, 0);
  lp.colLowerBound(sb, 0);
  lp.colLowerBound(sa, 0);

  //2. nao negatividade das contas
  for(int i=0; i<13; i++) {
    lp.addRow(saldoP[i] >= 0);
    lp.addRow(saldoCDB[i] >= 0);
    lp.addRow(saldoA[i] >= 0);
    lp.addRow(saldoCC[i] >= 0);
  }

  //3. soma das aplicacoes e despesas devem ser no maximo valor disponivel na CC
  for (int i=0; i<12; i++) {
    lp.addRow((ap[i] + ab[i] + aa[i] + dmTotal[i]) <= saldoCC[i]);
  }
}

int main(int argc, const char *argv[]) {

  scanf("%d\n", &n);    //numero de casos
  for (int caso=1; caso<=n; caso++) {
    // Leitura dos dados de entrada
    scanf("%lf %lf %lf %lf %lf\n", &h, &c, &rp, &tp, &tb);
    for (int i=0; i<12; i++) {  //rendimentos do CDB a cada mes
      scanf("%lf", &rb[i]);
    }
    scanf("\n");
    for (int i=0; i<12; i++) {  //rendimentos das acoes a cada mes
      scanf("%lf", &ra[i]);
    }
    scanf("\n");
    for (int i=0; i<12; i++) {  //despesas a cada mes
      scanf("%lf", &d[i]);
    }
    scanf("\n");
    d[12] = 0.0;                //nao ha despesas para o mes 1/2019

    // Resolucao do pl
    defineLP();
    lp.solve();

    cout.precision(2);        //definicao da precisao usada na impressao
    cout << fixed;

    // Impressao do resultado
    cout << "Caso " << caso << ":\n";
    if (lp.primalType() != Lp::OPTIMAL) {   //nao ha solucao otima
      cout << "Solucao impossivel ou ilimitada" << '\n';
    }
    else {  //imprime solucao otima obtida
      for (int i=0; i<12; i++) {
        cout << "Mes " << i+1 << "/2018:" << '\n';
        cout << "investir na poupanca " << 0.0 + lp.primal(ap[i]) + 0.0 << " dinheiros" << '\n';
        cout << "retirar da poupanca " << 0.0 + lp.primal(sp[i]) + 0.0 << " dinheiros" << '\n';
        cout << "investir no CDB " << 0.0 + lp.primal(ab[i]) + 0.0 << " dinheiros" << '\n';
        cout << "retirar do CDB " << 0.0 + lp.primal(sb[i]) + 0.0 << " dinheiros" << '\n';
        cout << "investir nas acoes " << 0.0 + lp.primal(aa[i]) + 0.0 << " dinheiros" << '\n';
        cout << "retirar das acoes " << 0.0 + lp.primal(sa[i]) + 0.0 << " dinheiros" << '\n';
      }
      cout << "Mes 1/2019:" << '\n';
      cout << "retirar da poupanca " << 0.0 + lp.primal(sp[12]) + 0.0 << " dinheiros" << '\n';
      cout << "retirar do CDB " << 0.0 + lp.primal(sb[12]) + 0.0 << " dinheiros" << '\n';
      cout << "retirar das acoes " << 0.0 + lp.primal(sa[12]) + 0.0 << " dinheiros" << '\n';
      cout << "saldo da heranca em 2019: " << 0.0 + lp.primal() + 0.0 << " dinheiros" << '\n';
    }//end impressao do resultado
  }//end casos
}//end main function
