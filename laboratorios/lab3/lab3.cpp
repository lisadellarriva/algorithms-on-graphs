// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 3: arvore geradora minima
// Aluna: Elisa Dell'Arriva - 135551

// MODELAGEM
// Vertices: cada distrito
// Arestas: encanamento entre dois distritos
// Funcao peso: custo do encanamento entre dois distritos
// Representacao: listas de adjacencias: um <vector> para o Vetor Adj e, um <vector> para cada lista de vizinhos

// SOLUCAO
// Seja G = (V, E) com função custo c(u,v): E->R o grafo que modela o problema.
// 1. Criar um "distrito virtual", r, tal que para todo v pertencente a V(G), o custo de (r,v) é o custo de ter um reservatório em v.
// 2. Rodar algoritmo de Prim no grafo G, obtendo a AGM.
// 3. Somar o peso de todas as arestas pertencentes à AGM de G.

// Analise
// [O(n+m)] Parte 1. Enquanto faz a leitura, montar grafo (lista de adjacências)
// [O(n+m)] Parte 2. Rodar DFS somando as celulas com a logica de ordenacao topologica
//                   - se encontrar uma aresta de retorno: imprimir "erro circular" e parar o programa
// [O(n)]   Parte 3. imprimir os valores finais das celulas
// Complexidade: O(n+m)

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <list>
#include <iostream>
#include <utility>
#include <algorithm>

using namespace std;

// ESTRUTURAS
const int INFINITO = (10^3) + 1;
/* Representacao de um vertice */
typedef struct vertex {
  int id;                           // identificador do vertice
  int primKey;                      // custo associado ao vértice
  struct vertex *mae;               // vertice antecessor
  int visited;
  list< pair<struct vertex*, int> > vizinhos;    // lista de vizinhos (adjacencias)
} Vertex, *VertexPtr;

// VARIAVEIS GLOBAIS
int N,                   // numero de destritos (|V(G)|)
    M;                   // numero de ligacoes entre destritos
vector<VertexPtr> Adj;   // Vetor de adjacencias
vector<int> custo;

void criaGrafo() {
    int u, v, c;
    Adj.resize(N+1);
    custo.resize(N+1);

    // Vertice virtual (r)
    Adj[0] = (Vertex*) malloc(sizeof(Vertex));
    Adj[0]->id = 0;
    Adj[0]->mae = NULL;
    Adj[0]->primKey = 0;
    Adj[0]->visited = 0;
    custo[0] = 0;
    // Adj[0]->vizinhos = NULL;

    for (int i=1; i <= N; i++) {
      Adj[i] = (Vertex*) malloc(sizeof(Vertex));
      Adj[i]->id = i;
      Adj[i]->mae = NULL;
      Adj[i]->primKey = INFINITO;
      Adj[i]->visited = 0;
      custo[i] = 0;
      // Adj[i]->vizinhos = NULL;

      // adicionar uma aresta (r,i) com peso lido
      scanf("%d\n", &c);
      Adj[0]->vizinhos.push_back(make_pair(Adj[i], c));
    }

    // ler as arestas e adicionar nas listas vizinhos adequadas
    for (int i=0; i < M; i++) {
      scanf("%d %d %d\n", &u, &v, &c);
      Adj[u]->vizinhos.push_back(make_pair(Adj[v], c));
      Adj[v]->vizinhos.push_back(make_pair(Adj[u], c));
    }
}

int vertexComparison(VertexPtr v1, VertexPtr v2) {
  if (v1->primKey > v2->primKey) {
    return 1;
  }
  return 0;
}

void AGM_Prim() {
  VertexPtr u, v;
  list< pair<VertexPtr, int> > aux;

  make_heap(Adj.begin(), Adj.end());

  for (int i=0; i <= N; i++) {
    u = Adj.front();
    u->visited = 1;
    aux = u->vizinhos;
    while (aux != NULL) {
      v = aux.first;
      if ((v->visited == 0) && (aux.second < v->primKey)) {
        v->mae = u;
        v->primKey = aux.second;
        custo[v->id] = aux.second;
      }
      aux = aux
    }
    pop_heap(Adj.begin(), Adj.end());
  }
}

int main(int argc, char const *argv[]) {
  int custoTotal = 0;

  scanf("%d %d\n", &N, &M);
  criaGrafo();

  AGM_Prim();

  for (int i=0; i <= N; i++) {
    custoTotal = custoTotal + custo[i];
  }

  printf("%d\n", custoTotal);
}
