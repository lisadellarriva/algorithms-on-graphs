// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 3: arvore geradora minima
// Aluna: Elisa Dell'Arriva - 135551

// MODELAGEM
// Vertices: cada distrito
// Arestas: encanamento entre dois distritos
// Funcao peso: custo do encanamento entre dois distritos
// Representacao: listas de adjacencias: um <vector> do tipo Vertex para o Vetor Adj e, um <vector> do tipo Edge para as arestas

// SOLUCAO
// Seja G = (V, E) com função custo c(u,v): E->R o grafo que modela o problema.
// 1. Criar um "distrito virtual", r, tal que para todo v pertencente a V(G), o custo de (r,v) é o custo de ter um reservatório em v.
// 2. Rodar algoritmo de Kruskal no grafo G, obtendo o custo total de uma AGM de G.

// Argumento de correcao
// Ao criar um distrito virtual, r, reduzimos o problema ao problema de encontrar o custo da agm
// para um grafo com peso apenas nas arestas.
// Usando o algoritmo de Kruskal, conseguimos encontrar esse valor corretamente.

// Analise
// [O(n+m)]       Parte 1. Enquanto faz a leitura, montar grafo (lista de adjacências)
// [O((n+m)lg(n)] Parte 2. Rodar Algoritmo de Kruskal
// Complexidade: O((n+m)lg(n))

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <utility>
#include <algorithm>

using namespace std;

// ESTRUTURAS
const int INFINITO = ((10*10*10) + 1);
/* Representacao de uma aresta */
typedef struct edge {
  int orig;                        // extremo da aresta
  int dest;                        // extremo da aresta
  int custo;                       // custo associado a aresta
} Edge, *EdgePtr;

/* Representacao de um vertice */
typedef struct vertex {
  int id;                          // identificador do vertice (a qual conjunto pertence)
  // struct vertex *mae;           // vertice antecessor
  // vector<EdgePtr> vizinhos;     // lista de adjacencias do vertice
} Vertex, *VertexPtr;

// VARIAVEIS GLOBAIS
int N,                                    // numero de destritos (|V(G)|)
    M;                                    // numero de ligacoes entre destritos
vector<VertexPtr> Adj;                    // Vetor de adjacencias
vector<EdgePtr> arestas;                  // vetor de arestas
vector< vector<VertexPtr> > vertex_sets;  // vetor de conjuntos disjuntos

void criaGrafo() {
  // printf("criaGrafo\n");
    int u, v, c;
    EdgePtr aux = NULL;

    Adj.resize(N+1);
    vertex_sets.resize(N+1);

    // criacao do distrito virtual (r)
    Adj[0] = (Vertex*) malloc(sizeof(Vertex));
    Adj[0]->id = 0;
    vertex_sets[0].push_back(Adj[0]);

    // leitura dos n distritos
    for (int i=1; i <= N; i++) {
      Adj[i] = (Vertex*) malloc(sizeof(Vertex));
      Adj[i]->id = i;
      vertex_sets[i].push_back(Adj[i]);

      // criacao das arestas (r, i)
      scanf("%d\n", &c);
      aux = (Edge*) malloc(sizeof(Edge));
      aux->orig = 0;
      aux->dest = i;
      aux->custo = c;
      arestas.push_back(aux);
    }

    // leitura das m arestas
    for (int i=0; i < M; i++) {
      scanf("%d %d %d\n", &u, &v, &c);

      aux = (Edge*) malloc(sizeof(Edge));
      aux->orig = u;
      aux->dest = v;
      aux->custo = c;
      arestas.push_back(aux);
    }
}

// Funcao de comparacao entre dois vertices - auxiliar à make_heap
int costComparison(EdgePtr e1, EdgePtr e2) {
  // printf("costComparison\n");
  if (e1->custo < e2->custo) {
    return 1;
  }
  return 0;
}

// une dois conjuntos disjuntos de vertices. [O(n+1)]
void vertex_sets_union(int u, int v) {
  int i = u, j = v;

  if (vertex_sets[u].size() > vertex_sets[v].size()) {
    i = v;
    j = u;
  }

  for (unsigned int k = 0; k < vertex_sets[i].size(); k++) {
    vertex_sets[i][k]->id = vertex_sets[j][0]->id;
    vertex_sets[j].push_back(vertex_sets[i][k]);
  }
}

// Algoritmo de Kruskal, baseado no pseudocodigo do livro do Cormen, 3 edicao
int AGM_Kruskal() {
  // printf("AGM_Kruskal\n");
  VertexPtr u, v;
  int custoTotal = 0;

  // ordenacao das arestas.
  sort(arestas.begin(), arestas.end(), costComparison);

  for (unsigned int i=0; i < arestas.size(); i++) {
    u = Adj[arestas[i]->orig];
    v = Adj[arestas[i]->dest];
    if (u->id != v->id) {
      custoTotal = custoTotal + arestas[i]->custo;
      vertex_sets_union(u->id, v->id);
    }
  }
  return custoTotal;
}

// funcao pricipal
int main(int argc, char const *argv[]) {
  scanf("%d %d\n", &N, &M);

  criaGrafo();

  printf("%d\n", AGM_Kruskal());
}
