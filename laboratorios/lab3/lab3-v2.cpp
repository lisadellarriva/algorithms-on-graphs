// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 3: arvore geradora minima
// Aluna: Elisa Dell'Arriva - 135551

// MODELAGEM
// Vertices: cada distrito
// Arestas: encanamento entre dois distritos
// Funcao peso: custo do encanamento entre dois distritos
// Representacao: listas de adjacencias: um <vector> do tipo Vertex para o Vetor Adj e, um <vector> do tipo Edge para cada lista de vizinhos

// SOLUCAO
// Seja G = (V, E) com função custo c(u,v): E->R o grafo que modela o problema.
// 1. Iniciar cada vertice com custo igual ao custo de construir um reservatorio nele
// 2. Rodar algoritmo de Prim no grafo G, obtendo a AGM.
// 3. Computar o peso total da AGM de G.

// Analise
// [O(n+m)]       Parte 1. Enquanto faz a leitura, montar grafo (lista de adjacências)
// [O((n+m)lg(n)] Parte 2. Rodar Algoritmo de Prim
// [O(n)]         Parte 3. Computar o custo total
// Complexidade: O((n+m)lg(n))

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <utility>
#include <algorithm>

using namespace std;

// ESTRUTURAS
const int INFINITO = ((10*10*10) + 1);
/* Representacao de uma aresta */
typedef struct edge {
  int dest;                        // extremo da aresta
  int peso;                        // custo associado a aresta
} Edge, *EdgePtr;

/* Representacao de um vertice */
typedef struct vertex {
  int id;                          // identificador do vertice
  int custo;                       // custo associado ao vértice
  struct vertex *mae;              // vertice antecessor
  int visited;
  vector<EdgePtr> vizinhos;        // lista de adjacencias do vertice
} Vertex, *VertexPtr;

// VARIAVEIS GLOBAIS
int N,                   // numero de destritos (|V(G)|)
    M;                   // numero de ligacoes entre destritos
vector<VertexPtr> Adj;   // Vetor de adjacencias

void criaGrafo() {
    int u, v, c;
    EdgePtr aux = NULL;

    Adj.resize(N);

    // leitura dos n distritos
    for (int i=0; i < N; i++) {
      scanf("%d\n", &c);

      Adj[i] = (Vertex*) malloc(sizeof(Vertex));
      Adj[i]->id = i+1;
      Adj[i]->mae = NULL;
      Adj[i]->custo = c;
      Adj[i]->visited = 0;
    }

    // leitura das m arestas
    for (int i=0; i < M; i++) {
      scanf("%d %d %d\n", &u, &v, &c);

      aux = (Edge*) malloc(sizeof(Edge));
      aux->dest = v-1;
      aux->peso = c;
      Adj[u-1]->vizinhos.push_back(aux);
      aux = (Edge*) malloc(sizeof(Edge));
      aux->dest = u-1;
      aux->peso = c;
      Adj[v-1]->vizinhos.push_back(aux);
    }
}

// Funcao de comparacao entre dois vertices - auxiliar à make_heap
int vertexComparison(VertexPtr v1, VertexPtr v2) {
  if (v1->custo >= v2->custo) {
    return 1;
  }
  return 0;
}

// Algoritmo de Prim, baseado no pseudocodigo do livro do Cormen, 3 edicao
void AGM_Prim() {
  VertexPtr u, v;
  vector<VertexPtr> Q (Adj);

  make_heap(Q.begin(), Q.end(), vertexComparison);
  while (Q.size() > 1) {
    u = Q.front();
    u->visited = 1;
    for (unsigned int k=0; k < u->vizinhos.size(); k++) {
      v = Adj[u->vizinhos[k]->dest];
      if ((v->visited == 0) && (u->vizinhos[k]->peso < v->custo)) {
        v->mae = u;
        v->custo = u->vizinhos[k]->peso;
      }
    }
    pop_heap(Q.begin(), Q.end(), vertexComparison);
    Q.pop_back();
  }
}

int main(int argc, char const *argv[]) {
  int custoTotal = 0;

  scanf("%d %d\n", &N, &M);
  criaGrafo();

  AGM_Prim();

  for (int i=0; i < N; i++) {
    custoTotal = custoTotal + Adj[i]->custo;
  }

  printf("%d\n", custoTotal);
}
