// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 4: Reducao entre problemas
// Aluna: Elisa Dell'Arriva - 135551

// REDUCAO
// Problema A: pedidos do Bar da Ines
// Problema B: caminho minimo em um DAG
// Queremos uma reducao A <(n²) B. Para isso, primeiramente precisamos modelar o
// problema A como um grafo.

// Tranformacao da entrada
// Cria um grafo G, da seguinte forma:
//    Vertices: representam os pedidos
//    Arestas: existe (u,v) em E(G) se o pedido v comeca depois do termino do pedido u
//    Funcao peso: w(u,v) <- (-1)*l(u,v), em que l(u,v) = lucro garantido pelo pedido v
// Cria dois vértices extras: I e F, de forma que:
//    para todo v em V(G)-{I,F}: existem (I,v) com peso w(I,v) e (v,F) com peso 0.

// Tranformacao da saida
// Imprime o lucro maximo (igual ao custo do caminho minimo * (-1)) e o número dos
// pedidos que garantem tal lucro.
// Complexidade: Como para cada pedido, checamos se ele conflita ou não com cada
//               um dos demais, temos complexidade O(n²).

// Argumento de correcao
// Ao criar os vertices I e F, reduzimos o problema a um grafo com peso apenas nas arestas.
// Como a funcao peso de G é o negativo do lucro dos pedidos, as arestas com os menores pesos
// correspondem aos maiores lucros e, portanto, um caminho minimo em G leva ao maximo lucro.
// Portanto, os pedidos que compoem o caminho minimo em G tambem garatem lucro maximo e, o
// valor de tal lucro é dado por: lucroMax = custoMin * (-1).
// Complexidade: Imprimir o lucro maximo é O(1) e imprimir os pedidos é O(n), então temos
//               complexidade O(n).

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>
#include "minpathcpp.h"

using namespace std;

//VARIAVEL GLOBAL
Grafo g(2); //grafo que modela o problema dos pedidos


// Funcao de tranformacao da entrada: cria um DAG com peso nas arestas
void tal_entrada() {
  int i, f, w;                   //auxiliares para leitura da entrada
  int n;                         //numero de pedidos
  vector<Grafo::Edge> pedidos;   //pedidos

  scanf("%d\n", &n);
  pedidos.resize(n+1);

  //cria um grafo de tamanho n+2
  for (int k=0; k < n; k++) {
    g.adicionaVertice();
  }

  //leitura da entrada
  for (int k=1; k <= n; k++) {
    scanf("%d %d %d\n", &i, &f, &w);

    pedidos[k].x = i;
    pedidos[k].y = f;
    pedidos[k].w = w;

    g.adicionaAresta(0, k, w*(-1));       //aresta do nó I ao nó k
    g.adicionaAresta(k, g.size()-1, 0);   //aresta do nó k ao nó F
  }

  //cria as arestas entre pedidos que não conflitam em horario
  for (int j=1; j <= n; j++) {
    for (int k=1; k <= n; k++) {
      if (pedidos[k].x > pedidos[j].y) {
        g.adicionaAresta(j, k, pedidos[k].w*(-1));
      }
    }
  }

}

//Funcao de transformacao da saida: imprime o lucro maximo e os referentes pedidos
void tal_saida(int custoMin, vector<Grafo::Edge> P) {
  cout << custoMin*(-1) << endl;      //lucro maximo e o negativo do custo minimo

  //imprime os pedidos que garatem o lucro maximo
  for (vector<Grafo::Edge>::iterator it = P.begin(); it != P.end()-1; it++) {
    cout << it->y << " ";
  }
  cout << endl;
}

int main(void) {
  vector<Grafo::Edge> P;            //caminho minimo no grafo G
  int custoMin;                     //custo do caminho minimo

  tal_entrada();
  custoMin = g.min_path(0, g.size()-1, P);
  tal_saida(custoMin, P);

  return 0;
}
