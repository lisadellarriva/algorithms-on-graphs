// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 5: Fluxo em redes
// Aluna: Elisa Dell'Arriva - 135551

// MODELAGEM
// Vertices: um vertice para cada dimensao da matriz, ou seja, N, uma fonte s e
//           um sorvedouro t.
// Arestas:  existe aresta xy em E(G) <=> x != y e (matriz[x][y] = 1 ou matriz[y][x] = 1) com c(xy) = 1;
//           existe aresta st em E(G) <=> x = y e matriz[x][y] = 1 com c(xy) = 1;
//           existe aresta sv(1) em E(G) com c(xy) = N;
//           existe aresta v(N)t em E(G) com c(xy) = N.

// SOLUCAO
// Modelamos o problema como um grafo direcionado, da maneira descrita acima.
// Dessa forma, se o fluxo maximo em G é menor que N, então a matriz nao é organizavel
// e, caso contrario, ela é organizavel.
// Suponha que o fluxo maximo em G é f, com |f| < N. Entao, existem no maximo |f|
// caminhos disjuntos de s a t em G. Como as arestas representam valor '1' na
// matriz, e todas tem capacidade igual a 1, cada caminho disjunto significa uma
// linha da matriz com valor '1' em uma coluna diferente. Como |f| < N, entao
// nao temos como colocar valor '1' em todas as colunas e, portanto, nao conseguimos
// reorganiza-la.
// Caso contrario, ou seja, |f| = N, entao temos como colocar valor '1' em todas
// as colunas e, portanto, conseguimos reorganizar a matriz.

// ANALISE DE COMPLEXIDADE DE TEMPO
// Leitura da entrada:  O(n^2)
// Construcao do grafo: O(n + m)
// Cada DFS:            O(n + m)
// getMaximumFlow:      O(n*m^2) (algoritmo baseado no de Edmonds-Karp)

// ANALISE DE COMPLEXIDADE DE MEMORIA
// |V(G)| = n + 2  => O(n)
// |E(G)| = m + 2 => O(n + m)
// Totalizando: O(n + m)

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <queue>
#include <iostream>
#include <utility>

using namespace std;

// ESTRUTURAS
const int CUSTO_UNIT = 1;
const int INFINITO = ((10*10*10) + 1);

/* Cores que representam os estados dos vertices na DFS */
typedef enum {
  BRANCO = 1, // vertice ainda nao visitado
  CINZA,      // vertice visitado
  PRETO       // vertice finalizado
} Cor;

/* Representacao de um vertice */
typedef struct vertex {
  int id;                       // identificador do vertice
  struct vertex *mae;           // vertice antecessor
  vector<int> vizinhos;         // lista de adjacencias do vertice
  Cor color;
  int d;
} Vertex, *VertexPtr;

// VARIAVEIS GLOBAIS
int N,                          // dimensao da matriz quadrada da entrada
    V;                          // |V(G)|
    // f = 0;                      // fluxo da rede

vector<VertexPtr> Adj;          // Vetor de adjacencias

// Cria o grafo G=(V, E) que modela o problema
void criaGrafo() {
  int e;

  scanf("%d\n", &N);            // leitura da dimensao da matriz
  V = N+2;                      // definicao do tamanho de v(G)

  Adj.resize(V);                // vetor de adjacencias

  // cria vértices
  for (int i=0; i<V; i++) {
    Adj[i] = (Vertex*) malloc(sizeof(Vertex));
    Adj[i]->id = i;
  }

  // cria arestas
  Adj[0]->vizinhos.push_back(1);    // aresta su com c(s,u) = N
  Adj[N]->vizinhos.push_back(V-1);  // aresta vt com c(v,t) = N
  for (int i=1; i <= N; i++) {      // arestas do tipo (uv) com c(u,v) = 1
    for (int j=1; j<= N; j++) {
      scanf("%d ", &e);
      if (e == 1) {
        if (i > j) {
            Adj[j]->vizinhos.push_back(i);
        }
        else if (i < j) {
          Adj[i]->vizinhos.push_back(j);
        }
        else {
          Adj[0]->vizinhos.push_back(V-1);  // em caso de loop, cria caminho direto de s a t
        }
      }
    }
  }

}

// Busca em largura no grafo G
// Retorna 1 se existe caminho de s a t e 0, caso contrário
int BFS() {
  queue<int> Q;
  int u;

  for (int i=1; i<V; i++) {
    Adj[i]->color = BRANCO;
    Adj[i]->d = INFINITO;
    Adj[i]->mae = NULL;
  }
  Adj[0]->color = CINZA;
  Adj[0]->d = 0;
  Adj[0]->mae = NULL;

  Q.push(0);
  while (!Q.empty()) {
    u = Q.front();
    Q.pop();
    // std::cout << "valor de u: "<< u << '\n';
    for (unsigned int v=0; v<Adj[u]->vizinhos.size(); v++) {
      if (Adj[Adj[u]->vizinhos[v]]->color == BRANCO) {
        Adj[Adj[u]->vizinhos[v]]->color = CINZA;
        Adj[Adj[u]->vizinhos[v]]->d = Adj[u]->d + 1;
        Adj[Adj[u]->vizinhos[v]]->mae = Adj[u];
        Q.push(Adj[Adj[u]->vizinhos[v]]->id);
      }
    }
    Adj[u]->color = PRETO;
  }

  // std::cout << "tamanho s a t: " << Adj[V-1]->d << '\n';

  if (Adj[V-1]->mae != NULL) {
    // std::cout << "vai retornar 1" << '\n';
    return 1;
  }
  // std::cout << "vai retornar 0" << '\n';
  return 0;
}

// Computa o valor do fluxo máximo no grafo G
// Implementacao baseada no pseudocodigo do algoritmo de Edmonds-Karp
// apresentado no livro Introduction to Algorithms, 3 edicao.
int getMaximumFlow() {
  int caminhoAumentante = 0;
  int f = 0;

  caminhoAumentante = BFS();
  while (caminhoAumentante != 0) {
    f = f + CUSTO_UNIT;
    // std::cout << "caminho numero " << f << '\n';
    // E(G) <- E(G)\E(P); (tem sempre 2 iteracoes)
    if (Adj[V-1]->mae->id == 0) { // caminho direto de s a t
      Adj[0]->vizinhos.pop_back();
    }
    else {// limitado pelo numero de arestas (O(m))
      for (VertexPtr p = Adj[V-1]->mae; p->id != 0; p = p->mae) {
        //em p->mae->vizinhos, procurar o p->id e excluir
        for (unsigned int k=0; k<p->mae->vizinhos.size(); k++) {
          if (Adj[p->mae->vizinhos[k]]->id == p->id) {
            p->mae->vizinhos.erase(p->mae->vizinhos.begin()+k);
            break;
          }
        }
      }
    }

    // std::cout << "antes da BFS" << '\n';
    caminhoAumentante = BFS();
    // std::cout << "depois da BFS" << '\n';
  }
  std::cout << "fluxo maximo: "<< f << '\n';
  return f;
}


// funcao pricipal
int main(int argc, char const *argv[]) {

  criaGrafo();

  if (getMaximumFlow() < N) {
    cout << "NAO" << endl;
  }
  else {
    cout << "SIM" << endl;
  }
}
