// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 5: Fluxo em redes
// Aluna: Elisa Dell'Arriva - 135551

// MODELAGEM
// Vertices: V = (L U C U {s,t}), em que L = {v : v é uma linha da matriz},
//                                       C = {v : v é uma coluna da matriz},
//                                       s é fonte e t é sorvedouro.
// Arestas:  existe aresta xy em E(G) <=> matriz[x][y] é 1;
//           existe aresta sv em E(G), para todo v em L;
//           existe aresta vt em E(G), para todo v em C.
// Capacidade das arestas: c(e) = 1, para todo e em E(G).

// SOLUCAO
// Modelamos o problema como um grafo bipartido, da seguinte maneira.
// Uma particao, L, contendo vertices que representam as linhas da matriz e, uma
// particao, C, que representam as colunas da matriz. Criamos aresta uv entre
// dois vertices sse o valor da matriz na posicao (u,v) é 1. Dessa forma, se
// existe um emparelhamento perfeito em (L U C), então a matriz é organizavel.
// Suponha que existe emparelhamento perfeito, M contido em E(G). Seja uv em E(G)
// uma aresta arbitraria. Como G é direcionado e bipartido em particoes L e C,
// sabemos que u pertence a L e v pertence a C e, pela construcao de G, sabemos
// que a aresta uv significa que a linha u da matriz tem valor '1' na coluna v.
// Como M é emparelhamento, para toda aresta xy em E(G) com x != u, temos y != v.
// Como uv é arbitraria, vale para toda aresta em E(G). Logo, as extremidades
// dos pares nao se repetem e, portanto, cada linha tem valor '1' em pelo menos
// uma coluna diferente. Como M é perfeito, não exite vertice não emparelhado e
// o resultado segue.
// Finalmente, para resolver o problema de emparelhamento perfeito, reduzimo-no
// ao problema de fluxo maximo, como visto e provado em exercicio anterior.

// ANALISE DE COMPLEXIDADE DE TEMPO
// Leitura da entrada:  O(n^2)
// Construcao do grafo: O(2n + m)
// Cada DFS:            O(n + m)
// getMaximumFlow:      O(n*m^2) (algoritmo baseado no de Edmonds-Karp)
// Totalizando:  O(n*m^2)

// ANALISE DE COMPLEXIDADE DE MEMORIA
// |V(G)| = 2*n + 2  => O(n)
// |E(G)| = 2*n + m  => O(n + m)
// Totalizando: O(n + m)


#include <cstdio>
#include <cstdlib>
#include <vector>
#include <queue>
#include <iostream>
#include <utility>

using namespace std;

// ESTRUTURAS
const int CUSTO_UNIT = 1;
const int INFINITO = ((10*10*10) + 1);

/* Cores que representam os estados dos vertices na DFS */
typedef enum {
  BRANCO = 1, // vertice ainda nao visitado
  CINZA,      // vertice visitado
  PRETO       // vertice finalizado
} Cor;

/* Representacao de um vertice */
typedef struct vertex {
  int id;                       // identificador do vertice
  struct vertex *mae;           // vertice antecessor
  vector<int> vizinhos;         // lista de adjacencias do vertice
  Cor color;
  int d;
} Vertex, *VertexPtr;

// VARIAVEIS GLOBAIS
int N,                          // dimensao da matriz quadrada da entrada
    V;                          // |V(G)|
    // f = 0;                      // fluxo da rede

vector<VertexPtr> Adj;          // Vetor de adjacencias

// Cria o grafo G=(V, E) que modela o problema
void criaGrafo() {
  int e;

  scanf("%d\n", &N);            // leitura da dimensao da matriz
  V = 2*N + 2;                  // definicao do tamanho de v(G)

  Adj.resize(V);                // vetor de adjacencias

  // cria vertice fonte, s
  Adj[0] = (Vertex*) malloc(sizeof(Vertex));
  Adj[0]->id = 0;

  // cria vértices da particao L
  for (int i=1; i<=N; i++) {
    Adj[i] = (Vertex*) malloc(sizeof(Vertex));
    Adj[i]->id = i;

    Adj[0]->vizinhos.push_back(i);   // arestas do tipo (s,v), para todo v em L
  }

  // cria vertice sorvedouro, t
  Adj[V-1] = (Vertex*) malloc(sizeof(Vertex));
  Adj[V-1]->id = V-1;

  // cria vértices da particao C
  for (int i=N+1; i<V-1; i++) {
    Adj[i] = (Vertex*) malloc(sizeof(Vertex));
    Adj[i]->id = i;

    Adj[i]->vizinhos.push_back(V-1);   // arestas do tipo (v, t), para todo v em C
  }

  // cria arestas do tipo (uv) com u em L e v em C
  for (int i=1; i <= N; i++) {
    for (int j=1; j<= N; j++) {
      scanf("%d ", &e);
      if (e == 1) {
        Adj[i]->vizinhos.push_back(j+N);
      }
    }
  }

}

// Busca em largura no grafo G
// Retorna 1 se existe caminho de s a t e 0, caso contrário
int BFS() {
  queue<int> Q;
  int u;

  for (int i=1; i<V; i++) {
    Adj[i]->color = BRANCO;
    Adj[i]->d = INFINITO;
    Adj[i]->mae = NULL;
  }
  Adj[0]->color = CINZA;
  Adj[0]->d = 0;
  Adj[0]->mae = NULL;

  Q.push(0);
  while (!Q.empty()) {
    u = Q.front();
    Q.pop();
    // std::cout << "valor de u: "<< u << '\n';
    for (unsigned int v=0; v<Adj[u]->vizinhos.size(); v++) {
      if (Adj[Adj[u]->vizinhos[v]]->color == BRANCO) {
        Adj[Adj[u]->vizinhos[v]]->color = CINZA;
        Adj[Adj[u]->vizinhos[v]]->d = Adj[u]->d + 1;
        Adj[Adj[u]->vizinhos[v]]->mae = Adj[u];
        Q.push(Adj[Adj[u]->vizinhos[v]]->id);
      }
    }
    Adj[u]->color = PRETO;
  }

  // std::cout << "tamanho s a t: " << Adj[V-1]->d << '\n';

  if (Adj[V-1]->mae != NULL) {
    // std::cout << "vai retornar 1" << '\n';
    return 1;
  }
  // std::cout << "vai retornar 0" << '\n';
  return 0;
}

// Computa o valor do fluxo máximo no grafo G
// Implementacao baseada no pseudocodigo do algoritmo de Edmonds-Karp
// apresentado no livro Introduction to Algorithms, 3 edicao.
int getMaximumFlow() {
  int caminhoAumentante = 0;
  int f = 0;

  caminhoAumentante = BFS();
  while (caminhoAumentante != 0) {
    f = f + CUSTO_UNIT;
    // std::cout << "caminho numero " << f << '\n';
    // E(G) <- E(G)\E(P); (tem sempre 2 iteracoes)
    for (VertexPtr p = Adj[V-1]->mae; p->id != 0; p = p->mae) {
      p->vizinhos.clear();
    }
    // std::cout << "antes da BFS" << '\n';
    caminhoAumentante = BFS();
    // std::cout << "depois da BFS" << '\n';
  }
  return f;
}


// funcao pricipal
int main(int argc, char const *argv[]) {

  criaGrafo();

  if (getMaximumFlow() < N) {
    cout << "NAO" << endl;
  }
  else {
    cout << "SIM" << endl;
  }
}
