// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 2: caminhos mínimos
// Aluna: Elisa Dell'Arriva - 135551

// MODELAGEM
// Vertices: cada cidade
// Arestas: sejam u,v vertices, existe aresta de u a v, se existe uma ferrovia ou uma estrada que liga u e v
// Representacao: listas de adjacencias: um <vector> de vertices para o Vetor Adj e, um <vector> de arestas para cada lista de vizinhos
//                vertice:  uma estrutura <vertex>
//                aresta:   uma estrutura <edge>, com atributos destino(vertex), tipo(bit)
//                grafo pi: um <vector> de arestas

// SOLUCAO
// Parte 1: Ao ler entrada, criar o grafo e inicializar as capitais com distancia 0.
// Parte 2: Rodar algoritmo de Dijkstra e gerar o grafo pi (de caminhos mpinimos).
// Parte 3: Contabilizar as ferrovias usadas no grafo pi e fazer a diferença com numero inicial de ferrovias.

// ANALISE


#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <utility>

using namespace std;
#define INFINITO 10^3+1

// ESTRUTURAS
/* Representacao de uma aresta */
typedef struct edge {
  int custo;                        // custo da aresta
  vector<struct vertex*> destino;   // extremidade destino da aresta
  int tipo;                         // 0 -> estrada; 1-> ferrovia
} Edge, *EdgePtr;

/* Representacao de um vertice */
typedef struct vertex {
  int id;                           // identificador do vertice
  int d;                            // estimativa de distancia
  vector<struct edge*> vizinhos;    // lista de vizinhos (adjacencias)
} Vertex, *VertexPtr;

// VARIAVEIS GLOBAIS
int n, m, p;
vector<VertexPtr> Adj;
vector<Edge> pi;

void criaGrafo(int N) {
    Adj.resize(N);
    for (int i=0; i < N; i++) {
      Adj[i] = (Vertex*) malloc(sizeof(Vertex));
      Adj[i]->id = i;
      Adj[i]->d = INFINITO;
      // Adj[i]->vizinhos = NULL;

      pi[i] = NULL;
    }
}

void Dijkstra() {
  
}

int main(int argc, char const *argv[]) {

  int i, j, c;
  EdgePtr e;

  //Parte 1: leitura da entrada e inicializacao do grafo
  scanf("%d %d %d\n", &n, &m, &p);
  pi.resize(n);
  criaGrafo(n);

  for (int i=0; i<m; i++) {
    scanf("%d %d %d\n", &i, &j, &c);
    e = (Edge*) malloc(sizeof(Edge));
    e->custo = c;
    e->destino = Adj[j];
    e->tipo = 0;
    Adj[i]->vizinhos.push_back(e);
  }

  for (int i=0; i<p; i++) {
    scanf("%d %d %d\n", &i, &j, &c);
    e = (Edge*) malloc(sizeof(Edge));
    e->custo = c;
    e->destino = Adj[j];
    e->tipo = 1;
    Adj[i]->d = 0;                  // i é uma capital
    Adj[i]->vizinhos.push_back(e);
  }

  //Parte 2: rodar Dijkstra
  Dijkstra();
}
