// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 1: busca em grafos
// Aluna: Elisa Dell'Arriva - 135551

// MINHA IDEIA SO FAR
// [O(n+m)] 1. Enquanto faz a leitura, montar grafo (lista de adjacência)
// [O(n+m)] 2. Rodar DFS com ord topológica (lista com inserção na cabeça)
//            - se encontrar uma aresta de retorno: imprimir "erro" e parar
// [O(n)]   3. imprimir a Ordenacao topologica

// MODELAGEM
// Vertices: cada celula da planilha
// Arestas: sejam u,v vertices, existe aresta de u a v, se o valor de u depende do valor em v
// Representacao: listas de adjacencias

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <deque>
#include <iostream>
#include <utility>

using namespace std;

// STRUCTURES
typedef enum {
  BRANCO = 1,
  CINZA,
  PRETO
} Cor;

typedef struct vertex {
  int id;
  int value;
  int f;
  int d;
  Cor cor;
  struct vertex *mae;  // vertice antecessor
  vector<struct vertex*> vizinhos;
} Vertex, *VertexPtr;

// GLOBAL VARIABLES
// const string ERRO_CIRCULAR = "Erro! Referencia circular.";
int tempo = 0;           // contador do tempo para DFS
int N;                   // |V(G)|
vector<VertexPtr> Adj;   // Adjacency array

void imprimeErro() {
  printf("Erro! Referencia circular.");
  exit(-1);
}

/* Imprime o garfo no formato listas de Adjacencias
 * complexidade: O(V + E)
 */
void imprimeGrafo() {
  for (int i=0; i<N; i++) {
    std::cout << "[" << i+1 << " : " << Adj[i]->value << "]";
    for (int k=0; k<Adj[i]->vizinhos.size(); k++) {
      std::cout << " -> " << "[" << Adj[i]->vizinhos[k]->id << " : " << Adj[i]->vizinhos[k]->value << "]";
    }
    std::cout << '\n';
  }
}

void criaGrafo(int N) {
    std::cout << "criaGrafo" << '\n';
    Adj.resize(N);
    for (int i=0; i < N; i++) {
      Adj[i] = (Vertex*) malloc(sizeof(Vertex));
      Adj[i]->id = i;
      Adj[i]->value = 0;
      Adj[i]->d = 0;
      Adj[i]->f = 0;
      // Adj[i]->vizinhos = NULL;
    }
}

VertexPtr criaVizinho(int id) {
  std::cout << "criaVizinho" << '\n';
  VertexPtr aux = Adj[id-1];

  return aux;
}

void preencheListaAdj(int i, char* inputS) {
  std::cout << "preencheListaAdj" << '\n';
  int num, count;
  int r = sscanf(inputS, "%*[A+]%d %n", &num, &count);

  while (r > 0) {
    if (num == i+1) {   // caso um dos vizinhos tenha o mesmo id que o nó em questao,
      imprimeErro();    // imprime mensagem de erro circular
    }

    Adj[i]->vizinhos.push_back(criaVizinho(num));
    inputS = inputS + count;
    r = sscanf(inputS, "%*[A+]%d %n", &num, &count);
  }
}

void DFS_visita(int u, deque<VertexPtr> &ord) {
std::cout << "DFS_visita: [" << Adj[u]->id << " : " << Adj[u]->value << "]\n";
  tempo = tempo + 1;
  Adj[u]->d = tempo;
  Adj[u]->cor = CINZA;
  for (int v=0; v < Adj[u]->vizinhos.size(); v++) {
    std::cout << "cor do vizinho: " << Adj[u]->vizinhos[v]->cor << '\n';
    if (Adj[u]->vizinhos[v]->cor == BRANCO) {
      Adj[u]->vizinhos[v]->mae = Adj[u];
      DFS_visita(Adj[u]->vizinhos[v]->id, ord);
      Adj[u]->value += Adj[u]->vizinhos[v]->value;
    }
    else if (Adj[u]->vizinhos[v]->cor == CINZA) {
      // existe uma aresta de retorno (de v a u) e, portanto, um ciclo
      // entao, imprime mensagem de erro circular
      imprimeErro();
    }
    else {  // o vizinho e preto, ja tem valor, entao somamos
      Adj[u]->value += Adj[u]->vizinhos[v]->value;
    }
  }
  Adj[u]->cor = PRETO;
  tempo = tempo + 1;
  Adj[u]->f = tempo;
  ord.push_front(Adj[u]);
  std::cout << "saiu DFS_visita" << '\n';
}

/* Executa busca em profundida e gera uma ordenacao topologica do grafo,
 * se possivel. Baseado no pseudocodigo para DFS do livro do Cormen, edicao 9.
 * Caso seja encontrada uma aresta de retorno, imprime mensagem de erro circular.
 */
void DFS_OrdTopologica(deque<VertexPtr> &ord) {
std::cout << "DFS_OrdTopologica" << '\n';
  for (int i=0; i<N; i++) { // inicializacao dos vertices
    Adj[i]->cor = BRANCO;
    Adj[i]->mae = NULL;
  }
  tempo = 0;                // reset do tempo

  for (int i=0; i<N; i++) {
    if (Adj[i]->cor == BRANCO) {
      DFS_visita(Adj[i]->id, ord);
    }
  }
}

void imprimeOrdTopologica(deque<VertexPtr> &ord) {
  for (int i=ord.size()-1; i>=0; i--) {
    cout << "[" << ord[i]->id << " : " << ord[i]->value << "]\n";
  }
}

void imprimeOrdFinal() {
  for (int i=0; i < Adj.size(); i++) {
    cout << "[" << Adj[i]->id << " : " << Adj[i]->value << "]\n";
  }
}


int main(int argc, char const *argv[]) {
  // Leitura da entrada
  int inputI;
  char inputS[30];
  deque<VertexPtr> ordTopologica;

  scanf("%d\n", &N);        // Le numero de vertices do grafo
  if (N <= 0) {exit(1);}    // Caso 0 vertices, encerra programa

  criaGrafo(N);             // Cria o vetor de Adjacencias  - O(|V|)

  for (int i=0; i<N; i++) { // Le cada vertices e seus potenciais vizinhos
    if (scanf("%d\n", &inputI) < 1) {
      scanf("%s\n", inputS);
      preencheListaAdj(i, inputS);
    }
    else {
      Adj[i]->value = inputI;
    }
  }

  // imprimeGrafo();
  // rodar dfs, criando uma ord topologica, ja somando as celujlas
  // se n conseguir criar a ord topologica; erro
  DFS_OrdTopologica(ordTopologica);

  // imprimeOrdTopologica(ordTopologica);
  imprimeOrdFinal();

  return 0;
}
