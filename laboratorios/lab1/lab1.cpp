// MC558 - Projeto e Análise de Algoritmos II
// Projeto de programação 1: busca em grafos
// Aluna: Elisa Dell'Arriva - 135551

// MODELAGEM
// Vertices: cada celula da planilha
// Arestas: sejam u,v vertices, existe aresta de u a v, se o valor de u depende do valor em v
// Representacao: listas de adjacencias: um <vector> para o Vetor Adj e, um <vector> para cada lista de vizinhos

// Analise
// [O(n+m)] Parte 1. Enquanto faz a leitura, montar grafo (lista de adjacências)
// [O(n+m)] Parte 2. Rodar DFS somando as celulas com a logica de ordenacao topologica
//                   - se encontrar uma aresta de retorno: imprimir "erro circular" e parar o programa
// [O(n)]   Parte 3. imprimir os valores finais das celulas
// Complexidade: O(n+m)

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <utility>
#include <algorithm>

using namespace std;

// ESTRUTURAS
/* Cores que representam os estados dos vertices na DFS */
typedef enum {
  BRANCO = 1, // vertice ainda nao visitado
  CINZA,      // vertice visitado
  PRETO       // vertice finalizado
} Cor;

/* Representacao de um vertice */
typedef struct vertex {
  int id;                           // identificador do vertice
  int value;                        // Valor da celula representada pelo vertice
  int f;                            // tempo de finalizacao na DFS
  int d;                            // tempo de descoberta na DFS
  Cor cor;                          // cor do vertice na DFS
  // struct vertex *mae;               // vertice antecessor
  vector<struct vertex*> vizinhos;  // lista de vizinhos (adjacencias)
} Vertex, *VertexPtr;

// VARIAVEIS GLOBAIS
int tempo = 0;           // contador do tempo para DFS
int N;                   // |V(G)|
vector<VertexPtr> Adj;   // Vetor de adjacencias
int existeCiclo = 0;     // flag para identificar se existe ciclo

void imprimeErro() {
  printf("Erro! Referencia circular.\n");
}

/* Imprime o garfo no formato listas de Adjacencias.
 * Funcao auxiliar para debug.
 */
// void imprimeGrafo() {
//   for (int i=0; i<N; i++) {
//     std::cout << "[" << i+1 << " : " << Adj[i]->value << "]";
//     for (int k=0; k<Adj[i]->vizinhos.size(); k++) {
//       std::cout << " -> " << "[" << Adj[i]->vizinhos[k]->id << " : " << Adj[i]->vizinhos[k]->value << "]";
//     }
//     std::cout << '\n';
//   }
// }

void criaGrafo(int N) {
    Adj.resize(N);
    for (int i=0; i < N; i++) {
      Adj[i] = (Vertex*) malloc(sizeof(Vertex));
      Adj[i]->id = i;
      Adj[i]->value = 0;
      Adj[i]->d = 0;
      Adj[i]->f = 0;
      Adj[i]->cor = BRANCO;
      // Adj[i]->vizinhos = NULL;
    }
}

void preencheListaAdj(int i, char* inputS) {
  int num, count;
  int r = sscanf(inputS, "%*[A+]%d %n", &num, &count);

  while (r > 0) {
    Adj[i]->vizinhos.push_back(Adj[num-1]);
    inputS = inputS + count;
    r = sscanf(inputS, "%*[A+]%d %n", &num, &count);
  }
}

/* Executa busca em profundida - funcao auxiliar a DFS_OrdTopologica().
 * Caso seja encontrada uma aresta de retorno, imprime mensagem de erro circular.
 * Implementacao baseada no pseudocodigo para DFS do livro do Cormen, edicao 9.
 */
void DFS_visita(int u) {
  tempo = tempo + 1;
  Adj[u]->d = tempo;
  Adj[u]->cor = CINZA;

  for (int v=0; v < Adj[u]->vizinhos.size(); v++) { // explora a vizinhanca de u.
    if (Adj[u]->vizinhos[v]->cor == BRANCO) {       // vizinho ainda nao visitado
      // Adj[u]->vizinhos[v]->mae = Adj[u];            // seta o antecessor
      DFS_visita(Adj[u]->vizinhos[v]->id);          // visita recursivamente o vizinho em questao
      Adj[u]->value += Adj[u]->vizinhos[v]->value;  // na volta da recursao, o vizinho ja tem seu valor, entao somamos
    }
    else if (Adj[u]->vizinhos[v]->cor == CINZA) {   // existe uma aresta de retorno (de v a u) e, portanto, um ciclo
      existeCiclo++;                                // entao, imprime mensagem de erro circular.
    }
    else {                                          // o vizinho ja foi finalizado,
      Adj[u]->value += Adj[u]->vizinhos[v]->value;  // ou seja, ja tem seu valor, entao somamos
    }
  }

  Adj[u]->cor = PRETO;                              // configura valores para um vertice finalizado
  tempo = tempo + 1;
  Adj[u]->f = tempo;
}

/* Executa busca em profundidade.
 * Caso seja encontrada uma aresta de retorno, imprime mensagem de erro circular.
 * Implementacao baseada no pseudocodigo para DFS do livro do Cormen, edicao 9.
 * Complexidade: O(V + E).
 */
void DFS_OrdTopologica() {
  tempo = 0;                      // reset do tempo

  for (int i=0; i<N; i++) {       // para cada vertice ainda nao visitado,
    if (Adj[i]->cor == BRANCO) {  // visita-o e todos seus vizinhos.
      DFS_visita(Adj[i]->id);
    }
  }
}

/* Imprime o valor final de cada celula
 */
void imprimeOrdFinal() {
  for (int i=0; i < Adj.size(); i++) {
    cout << Adj[i]->value << "\n";
  }
}


int main(int argc, char const *argv[]) {
  int inputI;
  char inputS[300000];

  /* Parte 1: leitura da entrada e criacao do grafo */
  scanf("%d\n", &N);            // Le numero de vertices do grafo

  criaGrafo(N);                 // Cria o vetor de Adjacencias - O(|V|)
  for (int i=0; i<N; i++) {     // Le cada vertice e seus potenciais vizinhos
    if (scanf("%d\n", &inputI) < 1) {
      scanf("%s\n", inputS);
      preencheListaAdj(i, inputS);
    }
    else {
      Adj[i]->value = inputI;
    }
  }

  /* Parte 2: Executa DFS somando os valores das celulas */
  DFS_OrdTopologica();  // grafo ja esta incializado

  /* Parte 3: Imprime os valores finais de cada celula */
  if (!existeCiclo) {
    imprimeOrdFinal();
  }
  else {
    imprimeErro();
  }

  return 0;
}
